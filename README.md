# Change Log
## 20181030
 - 支持在consumer中实例化producer


Kafka-php local version
==========
created this repository by [zmisgod](https://github.com/zmisgod)

## Different from composer version v0.2.0.8 (与composer上的v0.2.0.8相比，有什么不同)
- fix [loss first message when consumer service restart] 修复了【消费者服务重启后首条消息丢失的bug】

## Why not clone the latest version from composer or github (为什么不用composer或者github最新的版本)
v0.2.0.8 is the latest version of [packagist](https://packagist.org/ ) V0.2.0.8是最新的版本（截止2018-7-21）

github's master branch fixed this bug, but master branch need php 7.1+ instead of php 7.0  github的master分支已经修复了这个bug，但是需要php7.1以上的版本，而我们公司用的是php7.0

## Why i clone this repository in our local repository (为什么我要克隆这个仓库到我们的本地仓库)
Because i found this problem and report to the official group, but they make no response to me. 

我发现这个问题，我就把这个问题报给官方团队，但是没人理我，只能自己修复。

## 文档

[中文文档](README_CH.md)

Kafka-php is a pure PHP kafka client that currently supports greater than 0.8.x version of Kafka, this project v0.2.x and v0.1.x are incompatible if using the original v0.1.x You can refer to the document 
[Kafka PHP v0.1.x Document](https://github.com/weiboad/kafka-php/blob/v0.1.6/README.md), but it is recommended to switch to v0.2.x . v0.2.x use PHP asynchronous implementation and kafka broker interaction, more stable than v0.1.x efficient, because the use of PHP language so do not compile any expansion can be used to reduce the access and maintenance costs


## Requirements

* Minimum PHP version: 5.5
* Kafka version greater than 0.8
* The consumer module needs kafka broker version  greater than 0.9.0

## Installation

Add the lib directory to the PHP include_path and use an autoloader like the one in the examples directory (the code follows the PEAR/Zend one-class-per-file convention).

## Composer Install

Simply add a dependency on nmred/kafka-php to your project's composer.json file if you use Composer to manage the dependencies of your project. Here is a minimal example of a composer.json file :

```
{
	"require": {
		"nmred/kafka-php": "0.2.*"
	}
}
```

## Configuration

Configuration properties are documented in [Configuration](docs/Configure.md)

## Producer

### Asynchronous mode

```php
<?php
require '../vendor/autoload.php';
date_default_timezone_set('PRC');
use Monolog\Logger;
use Monolog\Handler\StdoutHandler;
// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StdoutHandler());

$config = \Kafka\ProducerConfig::getInstance();
$config->setMetadataRefreshIntervalMs(10000);
$config->setMetadataBrokerList('10.13.4.159:9192');
$config->setBrokerVersion('0.9.0.1');
$config->setRequiredAck(1);
$config->setIsAsyn(false);
$config->setProduceInterval(500);
$producer = new \Kafka\Producer(function() {
	return array(
		array(
			'topic' => 'test',
			'value' => 'test....message.',
			'key' => 'testkey',
			),
	);
});
$producer->setLogger($logger);
$producer->success(function($result) {
	var_dump($result);
});
$producer->error(function($errorCode) {
		var_dump($errorCode);
});
$producer->send(true);
```

### Synchronous mode

```
<?php
require '../vendor/autoload.php';
date_default_timezone_set('PRC');
use Monolog\Logger;
use Monolog\Handler\StdoutHandler;
// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StdoutHandler());

$config = \Kafka\ProducerConfig::getInstance();
$config->setMetadataRefreshIntervalMs(10000);
$config->setMetadataBrokerList('127.0.0.1:9192');
$config->setBrokerVersion('0.9.0.1');
$config->setRequiredAck(1);
$config->setIsAsyn(false);
$config->setProduceInterval(500);
$producer = new \Kafka\Producer();
$producer->setLogger($logger);

for($i = 0; $i < 100; $i++) {
        $result = $producer->send(array(
                array(
                        'topic' => 'test1',
                        'value' => 'test1....message.',
                        'key' => '',
                ),
        ));
        var_dump($result);
}
```

## Consumer

```php
<?php
require '../vendor/autoload.php';
date_default_timezone_set('PRC');
use Monolog\Logger;
use Monolog\Handler\StdoutHandler;
// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new StdoutHandler());

$config = \Kafka\ConsumerConfig::getInstance();
$config->setMetadataRefreshIntervalMs(10000);
$config->setMetadataBrokerList('10.13.4.159:9192');
$config->setGroupId('test');
$config->setBrokerVersion('0.9.0.1');
$config->setTopics(array('test'));
//$config->setOffsetReset('earliest');
$config->setForkHeartbeatProcess(true); // 是否fork子进程发送心跳数据（适用于耗时较长的消费者）
$consumer = new \Kafka\Consumer();
$consumer->setLogger($logger);
$consumer->start(function($topic, $part, $message) {
	var_dump($message);
});
```
